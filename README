Stubber - mocking framework that helps

main features:
- ability to mock private method 
- fast mocking and spying using the decorator pattern - with just 
one line o'code(tm)
- excellent support for Fluent interfaces
- mocking of final classes and methods through duckMocking
- seamless intergation with PHPUnit 3.x (no necessary to do any ::close, setups 
whatsoever)
- ability to debug mocked code - the generated code gets emitted to a file and
then its included in terms of a file require.


before release 0.5:

- duckMock: mocking of final classes
- duckMock: mocking of final methods
- parameter introspection
- source extraction using tokenizer
- multiple extracts
- register returnReference
- register returnCallback
- syntax sugar like:
    $stubber->exampleMethod->called()->once true|false
    $stubber->exampleMethod->called()->onceWith($args, ...) true | false
    $stubber->exampleMethod->called()->never true|false
    $stubber->exampleMethod->called()->neverWith($args, ...) true|false
    $stubber->exampleMethod->called->with($args) true | false
    $stubber->exampleMethod->calledAt(2)->with($args, ...) true |false


Rationale - why PHPunit mocks are not enough.

The rationale behind writing unit tests should be very simple: doing it should
 make you go fast.
If you are able to deliever robust code without tests that's fine but I am not
such person and it's difficult to meet someone that delivers mainainable code.
I know that whenever I start to work on code without any test suite it's going
to be painful, unreliable and eventually it will make me regret I'd started the
whole thing in the first place.
In the PHP world PHPUnit is becoming a xUnit framework of choice.
Unfortunaletely in the real world durning development I'm constantly coming
across legacy code full of dependencies that have to be broken. In such code the
lack of any automated testing procedures is a standard. If you want to start
with tests there a robust mocking framework (or an isolation framework) is a
must. The mocking capabilities of PHPUnit are quite good if not excellent but
its easiness is not good enough if you want to start quickly. Also as far as I
could dig into PHPMock usage and its internals if you want to use them to
isolate and just stub its very awkward to use. 
We all know PHP is not a perfect lanuguage. Many of its features affect
testability badly. Here a list of a few things that come into my mind:

- inability to redefine a function at runtime result in a fatal error - you have
to define proxy classes so that you can subclass when testing to alter the
behavior of built-in functions but few programmers actually bother to do so in
the existing codebases I have come accross,

- introduction of type hints: it just makes you artifically subclass to match
the hint - PHP is not a compiled language. The hint will indicate the problem
only at runtime. For the IDE's autocompletion why cannot we just get away just
with the annotations ?

- introduction of the final keyword - what if a type hint indicates a final type
parameter ? and too bad the Reflection extension is not able to manipulate the
final keyword - speaking of the Reflection extension: is not able to extract the
source code - it is just able to show you the start line and end line - not very
helpful if the script is not well formatted or all the definitions are on the
same line.
 