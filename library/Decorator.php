<?php
/*
Copyright (c) 2013-2014, Tomasz Cichecki
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Tomasz Cichecki nor the  names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * Automatically decorates a class
 * @author knight
 *
 */

class Decorator {
	private static $_file = array();
	protected static $_storeType = '\\Decorator\\Memory';
	protected static $_sourceCode = array();
	
	public static function setFileStore() {
		self::$_storeType = '\\Decorator\\TempFile\\Php';
	}
	
	public static function setMemoryStore() {
		self::$_storeType = '\\Decorator\\Memory';
	}

	public static function setDumpStore() {
		self::$_storeType = '\\Decorator\\Dump';
	}
	
	public static function decorate($instance, $type = 'Decorator\\DummyDecorator') {
		return self::decorateWith($instance, $type);
	}
	
	public static function decorateWith($instance, $type="Decorator\\DummyDecorator") {
		$decorating_class_name = self::generateDecorator($instance, null, $type, null);

		// availaible only as of PHP 5.4.x
		//$decorated = self::createNewInstanceWithoutConstructor($decorating_class_name);
		$decorated = new $decorating_class_name($instance);
		return $decorated;
	}
	
	protected static function createNewInstanceWithoutConstructor($className) {
		$reflector = Devello\ReflectionClass::getReflection($className);

		return $reflector->newInstanceWithoutConstructor();
	}
	
	public static function generateDecorator($forType, $new_type_id, $usedDecorator, $base_type=null) {
		if (is_string($forType) || ! $forType instanceof ReflectionClass) {
			$reflection = Devello\ReflectionClass::getReflection($forType);
		} else {
			$reflection = $forType;
		}
		if ($reflection->isAbstract()) {
			$adapter_id = uniqid();
			static::generateAdapter($reflection, $adapter_id);
			$adapterName = static::_adapterName($reflection, $adapter_id);
			if (!class_exists($adapterName))
				eval(static::$_sourceCode[$adapterName]);

			return static::generateDecorator($adapterName, $new_type_id, $usedDecorator);

		}
		if (!$new_type_id) {
			$new_type_id = static::_decoratorName($reflection, uniqid());
		}
		$decorating_class_name = $new_type_id;

		$extension = static::generateDecoratorSource($reflection, $base_type, $usedDecorator, $new_type_id);

		self::setSource($decorating_class_name, $extension);
		self::_eval($decorating_class_name, $extension);
		return $decorating_class_name;
	}

	/**
	 * @param ReflectionClass $reflection
	 * @param null $new_type_id
	 * @return string
	 */
	public static function generateAdapterSource($reflection, $new_type_id=null) {
		$methods = $reflection->getMethods(ReflectionMethod::IS_ABSTRACT);
		$source = '';

		$templates= \Devello\ReflectionClass::getReflection('Decorator\\Template\\Collection');
		$template = $templates->getMethod('__abstract_delegate_template__');
		foreach ($methods as $method) {
			$adapterMethod = static::_generateInstanceMethod($method, $template, array('abstract'=>'', 'final'=>''));
			$source .= $adapterMethod;
		}
		$source .= $templates->getMethod('_adapterInstance')->getSourceCode();
		$adapterName= static::_adapterName($reflection, $new_type_id);
		$relation = $reflection->isInterface() ? 'implements' : 'extends';
		$extension = sprintf(
			PHP_EOL . "class %s %s %s {" . PHP_EOL . "%s" . PHP_EOL . "}" . PHP_EOL,
			$adapterName, $relation, $reflection->getName(), $source);
		return $extension;
	}


	public static function generateAdapter($reflection, $new_type_id) {
		$source = self::generateAdapterSource($reflection, $new_type_id);
		$adapterName = static::_adapterName($reflection, $new_type_id);
		static::$_sourceCode[$adapterName] = $source;
		self::_eval($adapterName, $source);
		return $adapterName;
	}

	/**
	 * @param ReflectionClass $reflection
	 * @param $new_type_id
	 * @return mixed
	 */
	protected static function _adapterName($reflection, $new_type_id) {
		if (!$new_type_id) {
			$new_type_id = uniqid();
		}
		return static::_tempTypeName("Adapter_%s_For_%s", $reflection->getName(), $new_type_id);
	}

	protected static function _eval($decorating_class_name, $source) {
		$file = self::_storeSource($decorating_class_name, $source);
		$file->evalContent();
	}

	/**
	 * @param $decorating_class_name
	 * @param $source
	 * @return Decorator\SourceFile
	 */
	private static function _storeSource($decorating_class_name, $source) {
		$file = new self::$_storeType($decorating_class_name);
		
		self::$_file[] = $file;
		$file->store($source);
		return $file;
	}
	
	public static function setSource($class_name, $source) {
		self::$_sourceCode[$class_name] = $source;
	}

	public static function getSources() {
		return static::$_sourceCode;
	}
	
	public static function getSource($classNameOrInstance) {
		if (is_object($classNameOrInstance)) {
			return self::$_sourceCode[get_class($classNameOrInstance)];
		}
		return self::$_sourceCode[$classNameOrInstance];
	}

	/**
	 * @param Devello\ReflectionClass $reflected
	 * @return mixed
	 */
	public static function __get_source_for_type($reflected) {
		if (isset(self::$_sourceCode[$reflected->getName()])) {
			return self::getSource($reflected->getName());
		}
		return $reflected->getSourceCode();
	}

	/**
	 * @param ReflectionProperty $property
	 * @return string
	 */
	public static function __get_source_for_property($property) {
		return implode(" ", 
			Reflection::getModifierNames($property->getModifiers())) . 
			" $" . $property->getName() . ';';
	}

	/**
	 * @param ReflectionClass $reflection
	 * @param null $base_type
	 * @param string $type
	 * @param null $new_type_id
	 * @return mixed|string
	 */
	public static function generateDecoratorSource($reflection, $base_type=null, $type='Stubber', $new_type_id=null) {

		if (!$new_type_id) {
			$new_type_id = uniqid();
			$decorating_class_name = static::_decoratorName($reflection, $new_type_id);
		} else {
			$decorating_class_name = $new_type_id;
		}
		if (!$base_type) {
			$base_type = $reflection->getName();
		}

		if ($reflection->getMethods(\ReflectionMethod::IS_FINAL) || $reflection->isFinal()) {
			$base_type = 'stdClass';
			$interfaces = $reflection->getInterfaceNames();
			if ($interfaces) {
				$base_type .= ' implements ' . implode(', ', $interfaces);
			}
		}

		$decorating_reflection = \Devello\ReflectionClass::getReflection($type);

		if ($decorating_reflection->hasMethod("__get_class_source__")) {
			$decorating_reflection_source_method = $decorating_reflection->getMethod("__get_class_source__");
			$decorator_source = $decorating_reflection_source_method->invokeArgs(null, array());
		} else {


			$decorator_body = "";
			/** @var ReflectionProperty $property */
			foreach ($decorating_reflection->getProperties() as $property) {
				$declaring_class = $property->getDeclaringClass()->getName();
				if ($declaring_class != $decorating_reflection->getName()) {
					continue;
				}
				$decorator_body .= static::_getPropertySource($property);
			}

			/** @var Devello\ReflectionMethod $method */
			foreach($decorating_reflection->getMethods() as $method) {
				$declaring_class = $method->getDeclaringClass()->getName();
				if ($declaring_class != $decorating_reflection->getName()) {
					continue;
				}
				if (false !== strpos($method->getDocComment(), '@skipExtraction') ) {
					continue;
				}
				$decorator_body .= $method->getSourceCode();
			}

			$decorator_source = "class ". $decorating_reflection->getShortName() . " {
				$decorator_body
			}";
		}
		$source = static::generateMethodsSource($reflection, $type);

		return static::_createExtendedClassSource($decorating_class_name, $base_type, $source, $decorator_source);
	}

	/**
	 * @return string
	 * @param ReflectionProperty $property
	 */
	protected static function _getPropertySource($property) {
		if (!$property->isDefault()) {
			return "";
		}
		$modifiers = Reflection::getModifierNames($property->getModifiers());
		$source = implode(" ", $modifiers) . ' $' . $property->getName();

		$objectRef = $property->getDeclaringClass();
		$objectRef = new \Devello\ReflectionClass($objectRef->getName());
		$object = $objectRef->newInstanceWithoutConstructor();

		$property->setAccessible(true);
		$source .= " = " . var_export($property->getValue($object), true) . ";" . PHP_EOL;
		return $source;
	}


	protected static function _createExtendedClassSource($decorating_class_name, $base_type, $source, $decorator_source) {

		$extension = sprintf(PHP_EOL . "class %s extends %s {". PHP_EOL . "%s ", $decorating_class_name, $base_type, $source);
		$extension = preg_replace("/class\\s+[A-Za-z0-9_]+(\\s+extends\\s+[A-Za-z0-9_]+)?\\s+{/", $extension, $decorator_source . PHP_EOL, 1);
		return $extension;
	}

	/**
	 * @param ReflectionClass $reflection
	 * @param $new_type_id
	 * @return mixed
	 */
	protected static function _decoratorName($reflection, $new_type_id) {
		if (!$new_type_id) {
			$new_type_id = uniqid();
		}
		return static::_tempTypeName("Decorator_%s_For_%s", $reflection->getName(), $new_type_id);
	}

	protected static function _tempTypeName($name_template, $type_name, $new_type_id) {
		$name = sprintf($name_template, $new_type_id, $type_name);
		return str_ireplace('\\', '___', $name);
	}

	/**
	 * @param ReflectionClass $reflection
	 * @param $type
	 * @return string
	 */
	public static function generateMethodsSource($reflection, $type) {
		
		$decorator_reflection = Devello\ReflectionClass::getReflection($type);

		if ($decorator_reflection->hasMethod('__delegate_template__')) {
			$delegate_template = $decorator_reflection->getMethod('__delegate_template__');
		} else
			$delegate_template = Devello\ReflectionMethod::getReflection('\\Decorator\\Template\\Collection', '__delegate_template__');

		$filter_public = ReflectionMethod::IS_PUBLIC;
		$filter_private = ReflectionMethod::IS_PRIVATE;
		if (!$reflection->getMethods(ReflectionMethod::IS_FINAL)) {
			$filter_public |= ReflectionMethod::IS_PROTECTED;
		} else {
			$filter_private |= ReflectionMethod::IS_PROTECTED;
		}
		$public_methods = $reflection->getMethods($filter_public);
		$source = self::_generatePublicMethods(
			self::_getMethodsToDelegate($decorator_reflection, $public_methods),
			$delegate_template
		);

		if ($decorator_reflection->hasMethod('__delegate_template_private__')) {
			$delegate_template_private = $decorator_reflection->getMethod('__delegate_template_private__');
			$source .= self::_generatePrivateMethods(
				$reflection->getMethods($filter_private),
				$delegate_template_private
			);
		}

		return $source;
	}

	/**
	 * @param ReflectionClass $decorator_reflection
	 * @param $public_methods
	 * @return array
	 */
	private static function _getMethodsToDelegate($decorator_reflection, $public_methods) {
		$decorated = $decorator_reflection->getMethods(ReflectionMethod::IS_PUBLIC | ReflectionMethod::IS_PROTECTED);
		array_walk($decorated, function(ReflectionFunctionAbstract &$method) {
			$method = $method->getName();
		});
		$methods_to_generate  = array_filter($public_methods, function (ReflectionFunctionAbstract $method) use ($decorated) {
			return  !in_array($method->getName(), $decorated);
		});
		return $methods_to_generate;
	}

	/**
	 * @param array $reflection_methods
	 * @param $delegate_template
	 * @return string
	 */
	private static function _generatePublicMethods($reflection_methods, $delegate_template) {
		$source = static::_constructorTemplate();

		$methods = array_filter($reflection_methods, function(\Devello\ReflectionMethod $method) {
			return !$method->isStatic() && !$method->isMagic();
		});

		foreach ($methods as $method) {
			$delegate = self::_generatePublicMethod($method, $delegate_template);
			$source .= $delegate;
		}
		return $source;
	}

	private static function _generatePublicMethod($method, $template) {
		return self::_generateInstanceMethod($method, $template, array(
			'final'=>'', 'abstract'=>''
		));
	}

	private static function _constructorTemplate() {
		$reflection = Devello\ReflectionClass::getReflection('\\Decorator\\Template\\Collection');
		$constructor = $reflection->getMethod('__construct');
		return $constructor->getSourceCode();
	}

	/**
	 * @param array $reflection_methods
	 * @param $delegate_template
	 * @return string
	 */
	private static function _generatePrivateMethods($reflection_methods, $delegate_template) {
		$source = '';

		$methods = array_filter($reflection_methods, function(\Devello\ReflectionMethod $method) {
			return !$method->isStatic() && !$method->isMagic();
		});

		foreach ($methods as $method) {
			$delegate = self::_generatePrivateMethod($method, $delegate_template);
			$source .= $delegate;
		}
		return $source;
	}

	/**
	 * @param $method
	 * @param $delegate_template
	 * @return mixed
	 */
	private static function _generatePrivateMethod($method, $delegate_template) {
		return self::_generateInstanceMethod($method, $delegate_template, array(
			'private' => 'protected',
			'final' => '')
		);
	}

	protected static function _generateInstanceMethod($method, $delegate_template, $signatureOverrides=array()) {

		$delegate = self::_modifyTemplateParameters(
			$delegate_template,
			$method,
			$signatureOverrides
		);
		$delegate = self::_modifyReturnType($delegate, $method);
		return $delegate;
	}

	/**
	 * @param Devello\ReflectionMethod $template
	 * @param Devello\ReflectionMethod $method
	 * @param array $override
	 * @return mixed
	 */
	private static function _modifyTemplateParameters($template, $method, $override = array()) {
		$modified =  preg_replace(sprintf('/public\s+function\s*(%s)\((\$params)?\)/', $template->getName()), str_replace('$', '\\$', $method->methodSignature($override)), $template->getSourceCode(), 1);

		$passList = static::_parameterPassList($method);
		$modified = str_ireplace('$params', $passList, $modified);
		return $modified;
	}

	/**
	 * @param ReflectionFunctionAbstract $method
	 * @return string
	 */
	private static function _parameterPassList($method) {
		$parameters = $method->getParameters();
		$args = array();
		foreach ($parameters as $parameter) {
			$args[] = '$' . $parameter->getName();
		}

		return implode(", ", $args);
	}

	/**
	 * @param $source
	 * @param ReflectionFunctionAbstract $method
	 * @return mixed
	 */
	private static function _modifyReturnType($source, $method) {
		if($method->returnsReference()) {
			return preg_replace(
				'/=\\s*\$this\\-\\>_instance\\-\\>/',
				'= &$this->_instance->', $source);
		}
		return $source;
	}

}
