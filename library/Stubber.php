<?php
/*
Copyright (c) 2013-2014, Tomasz Cichecki
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Tomasz Cichecki nor the  names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

class Stubber extends Decorator {

	protected function __construct()
	{
		
	}
	/**
	 * @skipExtraction
	 */
	public static function __get_class_source__() {
		$reflection = Devello\ReflectionClass::getReflection('Stubber\\Doubles\\Spy');
		$source = 'class Stubber { %s }';
		$methods = $reflection->getMethods();
		$methods_array = array();
		foreach ($methods as $method) {
			if (false === strstr($method->getDocComment(), '@skipExtraction') ) {
				$methods_array[] = $method->getSourceCode();
			}
		}
		$properties = $reflection->getProperties();
		$properties_array = array();
		foreach ($properties as $property) {
			$properties_array[] = static::__get_source_for_property($property);
		}
		return sprintf(
			$source,
			implode(PHP_EOL , $methods_array) . PHP_EOL . implode(PHP_EOL, $properties_array));
	}
	
	/**
	 * @skipExtraction
	 */
	public static function spyOn($instance) {
		return static::decorateWith($instance, 'Stubber\\Doubles\\Spy');
	}
	
	/**
	 * 
	 * @param Object $instance
	 * @param $type
	 * @return Object
	 * 
	 * @skipExtraction
	 */
	public static function decorateWith($instance, $type='Stubber\\Doubles\\Spy') {
		$original_class_name = get_class($instance);
		$new_type_id = uniqid();

		$reflection = Devello\ReflectionClass::getReflection($original_class_name);
		$decorator_name = static::_decoratorName($reflection, $new_type_id);
		if (!$reflection->isEvaled()) {
			$proxy_class_name = static::generateProxy($reflection, $new_type_id);
			$decorating_class_name = static::generateDecorator($reflection, $decorator_name, $type, $proxy_class_name);

			if (!class_exists($decorating_class_name) ||
				!class_exists($proxy_class_name)) {
				return $instance;
			}
			$decorated = new $decorating_class_name;
			$decorated->_instance = new $proxy_class_name;
			$decorated->_instance->_instance = $decorated;
		} else {
			$decorating_class_name = static::generateDecorator($reflection, $decorator_name, $type);
			$decorated = new $decorating_class_name;
			$decorated->_instance = $instance;
		}
		$decorated->__bindFields($instance);
		return $decorated;
	}

	public static function generateProxy($reflection=null, $new_type_id) {
		$source = self::generateProxySource($reflection, $new_type_id);
		$proxyName = static::_proxyName($reflection, $new_type_id);
		static::$_sourceCode[$proxyName] = $source;
		self::_eval($proxyName, $source);
		return $proxyName;
	}

	public static function generateProxySource($reflection=null, $new_type_id=null) {
		$methods = $reflection->getMethods(ReflectionMethod::IS_PUBLIC | ReflectionMethod::IS_PROTECTED);
		$source = '';
		foreach ($methods as $method) {
			if ($method->isFinal()) {
				continue;
			}
			if (in_array($method->getName(), array('__construct'))) {
				$source .= "\tpublic function __construct() {}" . PHP_EOL;
				continue;
			}
			$tokens = token_get_all('<?php ' . $method->getSourceCode());
			array_walk($tokens, function(&$item) {
				if (is_array($item) && $item[1]=='$this') {
					$item[1]='$this->_instance';
				}
			});
			$method_source = array_reduce(array_slice($tokens, 1), function(&$result, $item) {
				if (!is_array($item)) {
					$result .=$item;
					return $result;
				}
				$result .= $item[1];
				return $result;

			}, "");
			$source .= PHP_EOL . "\t" . $method_source. PHP_EOL;
		}
		$proxyName = static::_proxyName($reflection, $new_type_id);
		$baseName = $reflection->getName();
		if ($reflection->isFinal()) {
			$baseName = 'stdClass';
		}

		$extension = sprintf(PHP_EOL . "class %s extends %s {" . PHP_EOL . "%s" . PHP_EOL . "}" . PHP_EOL, $proxyName, $baseName, $source);
		return $extension;
	}




	private static function _proxyName($reflection, $new_type_id) {
		return static::_tempTypeName("Proxy_%s_For_%s", $reflection->getName(), $new_type_id);
	}

	public static function disableAutoStubbing($instance) {
		$reflection = Devello\ReflectionClass::getReflection($instance);
		$property = $reflection->getProperty('_autoStubbing');
		$property->setAccessible(true);
		
		$property->setValue($instance, false);
	}

	public static function create($type='Stubber', $args=array()) {
		$instance = static::_instantiate($type, $args);
		return static::SpyOn($instance);
	}

	/**
	 * Creates a dummy for given type. A dummy do nothing, it just stays there...
	 * @param $forType
	 * @param array $args
	 * @return object
	 */
	public static function dummy($forType, $args=array()) {
		$instance = static::_instantiate($forType, $args);

		$stubber =static::decorateWith($instance, 'Stubber\\Doubles\\Dummy');
		return $stubber;
	}

	protected static function _instantiate($type, $args) {
		$reflection = Devello\ReflectionClass::getReflection($type);
		if ($reflection->isAbstract()) {
			$type = static::generateAdapter($reflection, uniqid());
			$reflection = Devello\ReflectionClass::getReflection($type);
		}

		if (!$reflection->isInstantiable()) {
			$instatiator = new \Instantiator\Instantiator();
			$instance = $instatiator->instantiate($reflection->getName());
		} else {
			if ($constructor = $reflection->getConstructor()) {
				$parameters = $constructor->getParameters();
				$mandatoryParameters = array_filter($parameters, function (\ReflectionParameter $parameter) {
					if ($parameter->isOptional()) {
						return false;
					}
					return true;
				});
				if (count($mandatoryParameters) > count($args)) {
					$instantiator = new \Instantiator\Instantiator();
					return $instantiator->instantiate($reflection->getName());
				}
			}
			$instance = $reflection->newInstanceArgs($args);
		}
		return $instance;
	}

	/**
	 * Creates a stub that can register return values for
	 * @param $forType
	 * @param array $args
	 * @return object
	 */
	public static function stub($forType, $args=array()) {
		$instance = static::_instantiate($forType, $args);

		$stub = static::decorateWith($instance, 'Stubber\\Doubles\\Stub');

		return $stub;
	}

}
