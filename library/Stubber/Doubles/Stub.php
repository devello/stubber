<?php
/**
Copyright (c) 2013-2014, Tomasz Cichecki
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Tomasz Cichecki nor the  names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

namespace Stubber\Doubles;


use Decorator, Stubber, ReflectionClass;

class Stub {

	private $_spy;
	public $_instance;
	protected $_autoStubbing = true;

	/**
	 * @skipExtraction
	 */
	public function __delegate_template__($params) {

		list($class, $method) = explode("::", __METHOD__);
		$return_value = $this->__unrestricted_call($method, array($params));

		return $return_value;
	}
	/**
	 * @skipExtraction
	 */
	public function __delegate_template_private__($params) {

		list($class, $method) = explode("::", __METHOD__);
		$return_value = $this->__unrestricted_call($method, array($params));

		return $return_value;
	}

	public function __call($method, $arguments) {
		$reflection = new ReflectionClass($this);
		if ($reflection->hasMethod($method)) {
			$m = $reflection->getMethod($method);
			if ($m->isProtected()) {
				throw new Stubber\Exceptions\AccessViolation;
			}
			if ($m->isPrivate()) {
				throw new Stubber\Exceptions\AccessViolation;
			}
		} elseif (false === $this->_autoStubbing) {
			throw new Stubber\Exceptions\UnknownMethod;
		}
		$return_value = $this->__unrestricted_call($method, $arguments);
		if (! $return_value instanceOf Stubber\Stub)
			return $return_value;
		return $this;
	}

	protected function __unrestricted_call($method, $arguments) {
		return call_user_func_array(array($this->_spy, $method), $arguments);
	}

	public function __isset($property) {
		return isset($this->_spy->$property);
	}

	public function __get($property) {
		return $this->_spy->$property;
	}

	public function __set($property, $value) {
		$this->_spy->$property = $value;
	}

	public function __bindFields($instance) {
		$instanceReflection = new ReflectionClass($instance);
		$properties = $instanceReflection->getProperties();
		if (!isset($this->_spy)) {
			$this->_spy = new Stubber\Stub($instance);
		}
		foreach ($properties as $property) {
			if ($property->isStatic()) {
				continue;
			}
			$prop_name = $property->getName();
			if ($property->isPublic() || $property->isProtected() && is_a($this, $instanceReflection->getName())) {
				$this->$prop_name =&$instance->$prop_name;
			}
		}
	}
}
