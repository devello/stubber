<?php
/*
Copyright (c) 2013-2014, Tomasz Cichecki
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Tomasz Cichecki nor the  names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
namespace Stubber;

class CallInfo {
	public $args = array();
	public $called = 0;

	public function __construct() {
		$this->invoked = $this;
	}

	/**
	 * Syntax sugar
	 * calledAt_<index>_with(<args>)
	 *
	 * @param $name
	 * @param $arguments
	 * @return bool
	 */
	public function __call($name, $arguments) {
		$calls = explode('_', $name);
		if (!method_exists($this, $calls[0]) || !method_exists($this, $calls[2])) {
			return false;
		}

		$matcher = new static();
		$callIndex = intval($calls[1]);
		$matcher->args[] = $this->args[$callIndex];

		$predicate = $calls[2];
		return 0 < call_user_func_array(array($matcher, $predicate), $arguments);
	}

	public function with() {
		$expected = func_get_args();
		return $this->withArguments($expected);
	}

	/**
	 * Returns the call count matching the $callSpec which is an array indexed
	 * by an argument indexes. Example:
	 * $spy->method(1, 2);  //the call
	 * $spy->method->with(1, 2); //returns 1 - an exact match
	 * $spy->method->withArguments(array(0=>1, 1=>2)); returns 1 - equivalent with the above
	 * $spy->method->withArguments(array(0=>1); //returns 1 because 'method' was called with 1 as the first argument
	 * $spy->method->withArguments(array(1=>2)); //returns 1 because method was called with 2 as the second argument
	 *
	 * @param array $callSpec
	 * @return int
	 */
	public function withArguments($callSpec) {
		$callCount = 0;
		foreach ($this->args as $args) {
			$spec = $callSpec;
			foreach($args as $argumentIndex=>$argValue) {
				if (!array_key_exists($argumentIndex, $spec)) {
					$spec[$argumentIndex] = $argValue;
				}
			}
			$diff = $this->_arrays_differ($args, $spec);
			if (!$diff) {
				++$callCount;
			}
		}
		return $callCount;
	}

	private function _arrays_differ($args, $expected) {
		$diff = array_diff($args, $expected);
		if ($diff) {
			return true;
		}

		foreach ($args as $key=>$value) {
			if (!array_key_exists($key, $args) || $args[$key] !== $expected[$key])
				return true;
		}
		return false;
	}

	public function called() {
		return $this->with() > 0;
	}

	/**
	 * Returns overall method call count
	 * @return int
	 */
	public function withAny() {
		return count($this->args);
	}

	/**
	 * @return bool
	 */
	public function neverCalledWith() {
		$notExpected = func_get_args();

		$callCount = call_user_func_array(array($this, 'with'), $notExpected);

		return $callCount == 0;
	}

	/**
	 * @return bool
	 */
	public function calledOnceWith() {
		return call_user_func_array(array($this, 'onceCalledWith'), func_get_args());
	}

	public function onceCalledWith() {
		$expected = func_get_args();

		$callCount = call_user_func_array(array($this, 'with'), $expected);

		return $callCount == 1;
	}

	public function calledAt() {
		$arguments = func_get_args();
		$matcher = new static();
		$matcher->args[] = $this->args[$arguments[0]];
		return $this;
	}
}
