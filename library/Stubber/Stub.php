<?php
/*
Copyright (c) 2013-2014, Tomasz Cichecki
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Tomasz Cichecki nor the  names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

namespace Stubber;

use ReflectionClass;

/**
 * Serialization product is used as means of objects identity
 * 
 * @param mixed $variable to be serialized
 * @return string
 */
function serialize($variable) {
    return \serialize(print_r($variable, true));
}

function locationMatches($location) {
	if (!$location) {
		return true;
	}

	if (!is_array($location[0])) {
		$location[0] = array('file'=>$location[0], 'line'=>$location[1]);
	}

	$numKeys = count($location[0]);
	if (!$numKeys) {
		return false;
	}

	$backTrace = array_slice(debug_backtrace(0), 8, 2);

	$backTrace[1]['line'] = $backTrace[0]['line'];
	foreach ($backTrace as $trace) {
		$matches = 0;
		foreach($location[0] as $locationKey=>$locationMatcher) {
			$matches += frameMatch($trace, $locationKey, $locationMatcher);
		}

		if ($matches == $numKeys) {
			return true;
		}
	}
	
	return false;
}

function frameMatch($trace, $locationKey, $locationMatcher) {
	if (!array_key_exists($locationKey, $trace)) {
		return 0;
	}

	if ($locationKey=='file') {
		if (false !== stripos($trace[$locationKey], $locationMatcher)) {
			return 1;
		}
	} else {
		if ($trace[$locationKey] == $locationMatcher) {
			return 1;
		}
	}
	return 0;
}

class Stub {
	protected $_spy = array(
		'calls'=>array(),
		'lastGet'=>null,
		'privates'=>null,
	);

	protected $_matchers = array();
	protected $_current = array(
		'method'=>null
	);


	public function __construct($spied=null) {
		if (is_null($spied)) {
			$spied = new \stdClass;
		}
		$this->_spy['privates'] = $spied;
	}

	public function returns_on_subsequent_calls() {
		$args = func_get_args();
		$this->_registerMatcher(array(
			'matcher'=>$this->_matcher($this->_current),
			'response'=>$this->_returns_on_subsequent_calls($args)
		));

		$this->_current = array('method'=>null);

		return $this;
	}
	
	private function _returns_on_subsequent_calls(&$args) {
		$self = $this;
		return  function() use (&$args, $self) {
			if (count ($args)) {
				return array_shift($args);
			}
			return $self;
		};
	}

	public function throws($argument) {
		$this->_registerMatcher(array(
			'matcher'=>$this->_matcher($this->_current),
			'response'=>function() use ($argument) {
				throw $argument;
			}
		));

		$this->_current = array('method'=>null);

		return $this;
	}

	public function stubs($callback) {
		$this->_registerMatcher(array(
			'matcher'=>$this->_matcher($this->_current),
			'response'=>function() use ($callback) {
				return call_user_func_array($callback, func_get_args());
			}
		));

		$this->_current = array('method'=>null);
		return $this;
	}

	public function returns($returnValue) {
		$this->_registerMatcher(array(
			'matcher'=>$this->_matcher($this->_current),
			'response'=>$this->_returns($returnValue, null)
		));

		$this->_current = array('method'=>null);

		return $this;
	}

	private function _registerMatcher($matcherResponse) {
		$this->_matchers[serialize($this->_current)] = $matcherResponse;
	}
	
	private function _matcher($current) {
		return function($method, $arguments) use ($current) {
			if ($method==$current['method']
				&& (empty($current['arguments']) || $current['arguments'] == serialize($arguments))
				&& (empty($current['location']) || locationMatches($current['location']))) {
				return true;
			}
			return false;
		};
	}

	private function _returns($argument, $location) {
		$self = $this;
		return function () use ($argument, $location, $self) {
			if (locationMatches($location)) {
				return $argument;
			}
			return $self;
		};
	}
	

	public function calledFrom($file, $line=null) {
		$this->_current['location'] = array($file, $line);
		return $this;
	}
	
	public function with() {
		$arguments = func_get_args();
		$this->_current['arguments'] = serialize($arguments);
		return $this;
	}

	public function __call($method, $arguments) {

		return $this->_response($method, $arguments);
	}

	protected function _response($method, $arguments) {
		foreach ($this->_matchers as $m) {
			$matcher = $m['matcher'];

			if ($matcher($method, $arguments)) {
				$response = $m['response'];
				return call_user_func_array($response, $arguments);
			}
		}
		return $this;
	}

	public function __get($property) {
		$value = $this->_peekProperty($property);
		if (!($value instanceOf Stub)) {
			return $value;
		}

		$this->_saveMethodName($property);
		return $this;
	}
	
	protected function _peekProperty($property) {
		if (isset($this->_spy['privates'])) {
			$reflector = new ReflectionClass($this->_spy['privates']);
			if ($reflector->hasProperty($property)) {
				$p = $reflector->getProperty($property);
				if (!$p->isStatic() && $p->isPrivate()) {
					$p->setAccessible(true);
					$v = $p->getValue($this->_spy['privates']);
					return $v;
				}
			}
			if (isset($this->_spy['privates']->$property)) {
				return $this->_spy['privates']->$property;
			}
		}
		return $this;
	}
	
	protected function _saveMethodName($methodName){
		$this->_spy['lastGet'] = $methodName;
		$this->_current['method'] = $methodName;
	}

	public function __isset($property) {
		if (!isset($this->_spy['privates'])) {
			return false;
		}
		$reflector = new ReflectionClass($this->_spy['privates']);
		if ($reflector->hasProperty($property)) {
			$p = $reflector->getProperty($property);
			if (!$p->isStatic() && $p->isPrivate()) {
				$p->setAccessible(true);
				$v = $p->getValue($this->_spy['privates']);
				if (!is_null($v)) {
					return true;
				}
				return false;
			}
		}
		return false;
	}

	public function __set($property, $value) {
		if (isset($this->_spy['privates'])) {
			$reflector = new ReflectionClass($this->_spy['privates']);
			if ($reflector->hasProperty($property)) {
				$p = $reflector->getProperty($property);
				if (!$p->isStatic() && $p->isPrivate()) {
					$p->setAccessible(true);
					$p->setValue($this->_spy['privates'], $value);
					return;
				}
			}
		}
		$this->$property = $value;
	}

}
