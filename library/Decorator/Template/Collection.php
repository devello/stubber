<?php
/*
Copyright (c) 2013-2014, Tomasz Cichecki
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Tomasz Cichecki nor the  names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
namespace Decorator\Template;

class Collection {

	private $_instance;

	public function __construct($instance = null) {

		if (get_parent_class($this)) {
			$parentName = get_parent_class($this);
			if (method_exists($parentName, '_adapterInstance')) {
				$instance = call_user_func_array(array($parentName, '_adapterInstance'), func_get_args());
			}
		}

		if (!$instance) {
			return;
		}

		$this->_instance = $instance;
		$instanceReflection = new \ReflectionClass($instance);
		$filter = ~\ReflectionProperty::IS_STATIC & ~\ReflectionProperty::IS_PRIVATE;
		if (!is_a($this, $instanceReflection->getName())) {
			$filter &= ~\ReflectionProperty::IS_PROTECTED;
		}
		$properties = $instanceReflection->getProperties($filter);
		foreach ($properties as $property) {
			$prop_name = $property->getName();
			$this->$prop_name =&$instance->$prop_name;
		}
	}

	public function __abstract_delegate_template__() {

	}

	public function __delegate_template__($params) {
		list (, $method) = explode("::", __METHOD__);
		$return_value = $this->_instance->$method($params);
		return $return_value;
	}

	public function _adapterInstance() {
		$args = func_get_args();
		$reflector = new \ReflectionClass(__CLASS__);
		return $reflector->newInstanceArgs($args);
	}

}
