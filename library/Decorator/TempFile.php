<?php
/*
Copyright (c) 2013-2014, Tomasz Cichecki
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Tomasz Cichecki nor the  names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
namespace Decorator;

class TempFile implements SourceFile {
	public $file;
	public function __construct($filename) {
		if (defined('TEMP_SOURCE_ROOT')) {
			$this->filename = TEMP_SOURCE_ROOT . DIRECTORY_SEPARATOR . $filename;
		} else {
			$this->filename = $filename;
		}
		$file = fopen($this->filename, 'w');
		$this->file = $file;
	}
	
	public function __destruct() {
		if (isset($this->file))
			@fclose($this->file);
		@unlink($this->filename);
	}
	
	public function store($source) {
		fwrite($this->file, $source);
		fflush($this->file);
		fclose($this->file);
		unset($this->file);
	}
	
	public function evalContent() {
		require_once $this->filename;
	}
}