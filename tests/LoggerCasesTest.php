<?php
/*
Copyright (c) 2013-2014, Tomasz Cichecki
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Tomasz Cichecki nor the  names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

use Examples\Logger, Examples\Service;

class LoggerCasesTest extends PHPUnit_Framework_TestCase
{
	public function testExampleOne() {
		$logger = new Logger();
		$infiltratedLogger = Stubber::spyOn($logger);
		$systemUnderTest = new Service();
		$systemUnderTest->setLogger($infiltratedLogger);
		
		//act
		$systemUnderTest->performJob();
		//assert
		$this->assertGreaterThan(0, $infiltratedLogger->log->invoked->with('an error occured'));
	}

	public function testExampleTwo() {
		$logger = new Logger();
		$spiedLogger = Stubber::spyOn($logger);
		$exceptionToBeThrown = new \Examples\Exceptions\ExampleException('Cannot log due to a filesystem error');
		$spiedLogger->log->throws($exceptionToBeThrown);
		$mockService = Stubber::create();

		try {

			$spiedLogger->log('this will blow it up!');

		} catch(\Examples\Exceptions\ExampleException $e) {

			$mockService->println(sprintf("A fatal exception occured: %s", $e->getMessage()));

		}
		$this->assertGreaterThan(
			0,
			$mockService->println->invoked->with("A fatal exception occured: Cannot log due to a filesystem error")
		);
	}

	public function testExampleThree()
	{
		$stubber = Stubber::create();

		$stubber->setProperty->stubs(function($value) use ($stubber) {
			$stubber->property = $value;
		});

		$stubber->setProperty('a value');

		$this->assertEquals('a value', $stubber->property);
	}
}
