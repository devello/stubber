<?php
namespace Examples;

class CallStack {
	protected $dependency;
	public function __construct($dependency) {
		$this->dependency = $dependency;
	}

	public function fromHere() {
		$fromHere = $this->dependency->method();
		$fromThere = $this->fromThere();
		if ($fromHere != $fromThere) {
			return $fromThere;
		}
		return $fromHere;
	}

	public function fromThere() {
		$fromThere = $this->dependency->method();
		return $fromThere;
	}
}