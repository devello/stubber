<?php
/*
Copyright (c) 2013-2014, Tomasz Cichecki
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Tomasz Cichecki nor the  names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
namespace Examples\Decorator\Proxy;
use stdClass;
class ArgumentsPassedByReference extends \Examples\ArgumentsPassedByReference {
	public function passedByReference(&$arg0) {

		list(, $method) = explode("::", __METHOD__);
		$return_value = $this->__unrestricted_call($method, array($arg0));
		if (isset($this->_returns[$method])) {
			return $return_value;
		}
		unset($return_value);
			
		$return_value = &call_user_func_array(array($this->_instance, $method), array($arg0));
		return $return_value;
	}
	public function &passedAndReturnedByReference(&$arg0) {

		list($class, $method) = explode("::", __METHOD__);
		$return_value = $this->__unrestricted_call($method, array($arg0));
		if (isset($this->_returns[$method])) {
			return $return_value;
		}
		//unset($return_value);
			
		$return_value = &$this->_instance->$method($arg0);
		//$return_value = &call_user_func_array(array($this->_instance, $method), array($arg0));
		return $return_value;
	}
	public function __call($method, $arguments) {
		$reflection = new ReflectionClass($this);
		if ($reflection->hasMethod($method)) {
			$m = $reflection->getMethod($method);
			if ($m->isProtected()) {
				throw new StubberAccessViolationException();
			}
			if ($m->isPrivate()) {
				throw new StubberAccessViolationException();
			}
		}
		return $this->__unrestricted_call($method, $arguments);
	}

	protected function __unrestricted_call($method, $arguments) {
		if (!isset($this->_returns)) {
			$this->_returns = array();
		}
		if (!isset($this->_calls)) {
			$this->_calls = array();
		}
		if (!isset($this->_argsMatcher)) {
			$this->_argsMatcher = array();
		}
		if ($method == 'returns') {
			$this->_returns[$this->_lastGet] = $arguments[0];
			return $this;
		}
		if ($method == 'with') {
			$this->_argsMatcher[$this->_lastGet] = $arguments;
			return $this;
		}
		if (!isset($this->_calls[$method])) {
			$callInfo = new stdClass;
			$callInfo->called = 1;
			$this->_calls[$method] = $callInfo;

		} else {
			$this->_calls[$method]->called++;
		}
		if (isset($this->_returns[$method]) && empty($this->_argsMatcher[$method])) {
			return $this->_returns[$method];
		}
		if (isset($this->_returns[$method]) && $this->_argsMatcher[$method] === $arguments) {
			return $this->_returns[$method];
		}
		return $this;
	}

	public function __get($property) {
		if (!isset($this->_calls)) {
			$this->_calls = array();
		}
		if ('called' == $property) {
			if (!isset($this->_calls[$this->_lastGet])) {
				return 0;
			}
			return $this->_calls[$this->_lastGet]->called;
		}
		$this->_lastGet = $property;
		return $this;
	}
}
