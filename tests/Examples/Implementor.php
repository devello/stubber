<?php

namespace Examples;

class Implementor implements AbstractTypes\Iface {
	public function method1() {}
	public function method2($a, Oneliner $b) {}
}
