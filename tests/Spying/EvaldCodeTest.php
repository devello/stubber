<?php

namespace Spying;
use PHPUnit_Framework_TestCase, Stubber;

class EvaldCodeTest extends PHPUnit_Framework_TestCase {
	public function testSpying_DynamicallyCreatedType_NoExceptionThrown()
	{
		$type = $this->getMock('stdClass', array('a', 'b'));
		Stubber::spyOn($type);
	}

	public function testSpying_DynamicallyCreatedTypeCalls_CallCountIsRecorded() {
		$type = $this->getMock('stdClass', array('a', 'b'));

		$spied = Stubber::spyOn($type);

		$spied->a();
		$spied->a();

		$call_count = $spied->a->called;
		$this->assertEquals(2, $call_count);
	}
}