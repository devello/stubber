<?php
/*
Copyright (c) 2013-2014, Tomasz Cichecki
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Tomasz Cichecki nor the  names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
namespace Spying;
use stdClass, Stubber;
use Examples;
use PHPUnit_Framework_TestCase;

class SpyingTest extends PHPUnit_Framework_TestCase {
	
	public static function assertThrows($expectedException, $code) {
		try {
			\call_user_func($code);
			self::fail(sprintf("No exception type: %s was thrown.", $expectedException));
		} catch (\Exception $e) {
			if ($e instanceof $expectedException) {
				return;
			}
			self::fail(sprintf("No exception type: %s was thrown.", $expectedException));
		}
	}
	
	public function callCountCases() {
		return array(
				array(0),
				array(1),
				array(2),
				array(10)
		);
	}
	
	
	public function testCountingCalls_SpiedInstance_CallcountForMethodCallsIsRecorded() {
		
		$stubber = Stubber::spyOn(new stdClass);

		$stubber->method();

		$this->assertEquals(1, $stubber->method->called);
	}
	
	public function testDisableStubbing_MethodDoesNotExistsInTheSpiedInstance_UnknownMethodExceptionThrown() {
		
		$stubber = Stubber::spyOn(new stdClass);
		Stubber::disableAutoStubbing($stubber);
		
		self::assertThrows('\\Stubber\\Exceptions\\UnknownMethod', function() use ($stubber) {
			$stubber->method();
		});
	}
	
	/**
	 * @dataProvider callCountCases
	 */
	public function testCountingCalls_InstanceMethodsCalledInternallyByTheInstance_MethodsAreCounted($expected_callcount) {
		$stubber = Stubber::spyOn(new Examples\OneCallsAnother);
	
		for ($i=0; $i<$expected_callcount; ++$i) {
			$stubber->method1();
		}

		$call_count = $stubber->method2->called;
	
		$this->assertEquals($expected_callcount, $call_count);
	}
	
	public function testStubbingReturnValue_StubbingPrivateMethodReturnValue_TheReturnValueCanBeStubbed() {
		$stubber = Stubber::spyOn(new Examples\ExampleWithPrivateMethods);

		$original_return_value = $stubber->method_public();
		$stubber->method_private->returns("this is stubbed!");

		$return_value_of_the_private_method = $stubber->method_public();
	
		$this->assertEquals("this is stubbed!", $return_value_of_the_private_method, "Stubber failed to stub private function return value");
		$this->assertNotEquals($original_return_value, $return_value_of_the_private_method);
	}
	
	public function testStubbingReturnValue_StubbingProtectedMethodReturnValue_TheReturnValueCanBeStubbed() {
		$stubber = Stubber::spyOn(new Examples\ExampleWithProtectedMethods());

		$original_return_value = $stubber->method_public();
		$stubber->method_protected->returns("this is stubbed!");

		$return_value_of_the_protected_method = $stubber->method_public();

		$this->assertEquals("this is stubbed!", $return_value_of_the_protected_method, "Stubber failed to stub protected function return value");
		$this->assertNotEquals($original_return_value, $return_value_of_the_protected_method);
	}
	
	public function testCallingInstancesMethods_DirectCallsToProtectedMethodsNotFromWithinTheInstance_AccessViolationIsThrown() {
		$stubber = Stubber::spyOn(new Examples\ExampleWithProtectedMethods());
		
		$this->assertThrows('\\Stubber\\Exceptions\\AccessViolation', function() use ($stubber) {
			$stubber->method_protected();
		});
	}
	
	public function testCallingInstancesMethods_DirecctCalssToProtectedMethodsNotFromWithinTheInstance_AccessViolationIsThrown() {
		$stubber = Stubber::spyOn(new Examples\ExampleWithPrivateMethods());

		$this->assertThrows('\\Stubber\\Exceptions\\AccessViolation', function() use ($stubber) {
			$stubber->method_private();
		});
	}

	/**
	 * test that documents php behavior regarding references
	 */
	public function testDocumentingPHP_ArgumentsPassedAndReturnedByReferenceUsingProxy() {
	
		$obj = new Examples\ProxyArgumentsPassedByReference();
		$value = 10;

		$ref_to_value = &$obj->passedAndReturnedByReference($value);

		$reflector = new \ReflectionClass('Examples\ProxyArgumentsPassedByReference');
		$method_reflection = $reflector->getMethod('passedAndReturnedByReference');

		$this->assertTrue($method_reflection->returnsReference());

		$ref_to_value += 1;
	
		$this->assertEquals(11, $value, 'Failure to document PHP behavior');
	}

	public function testCallingInstancesMethods_InstanceMethodThatReturnsReference_ReferenceIsUnaffectedAndCanBeUsed() {
		$stubber = Stubber::spyOn(new Examples\ArgumentsPassedByReference());
		$value = 10;

		$ref_to_value = &$stubber->passedAndReturnedByReference($value);
		$ref_to_value += 1;
	
		$this->assertEquals(11, $value, "The spy does not know how to pass by reference correctly");
	}
	
	public function testCallingInstancesMethods_CallingMethodsWhichTakesArgumentsByReference_VariableIsModifiedTheSameWayByTheSpiedObject() {
		$i = 0;
		$example = new Examples\ArgumentsPassedByReference();
		$example->passedByReference($i);
		$this->assertEquals(1, $i, 'Original class should modify the argument');
			
		$i = 0;
		$stubber = Stubber::spyOn(new Examples\ArgumentsPassedByReference());
		$stubber->passedByReference($i);
			
		$this->assertEquals(1, $i, 'Method should modify the argument passed');
	}
	
	/**
	 * @dataProvider callCountCases
	 */
	public function testCountingCalls_StubbingReturnValue_InternalCallsArePerformedAndCounted($expected_callcount) {
		$stubber = Stubber::spyOn(new Examples\ExampleWithManyMethods());

		$stubber->method1->returns('anything');
	
		for ($i=0; $i<$expected_callcount; ++$i) {
			$stubber->method1();
		}

		$callcount_method1 = $stubber->method1->called;
		$callcount_method2 = $stubber->method2->called;
		$callcount_method3 = $stubber->method3->called;

		$this->assertEquals($expected_callcount, $callcount_method1);
		$this->assertEquals($expected_callcount, $callcount_method2);
		$this->assertEquals(0, $callcount_method3);
	}
	
	public function testSpying_DecoratingInstanceWithFields_TheChangesToTheFieldsArePreserved() {
		$propertyObj = new Examples\ExampleProperty();
		$propertyObj->modifyProperty();
	
		$this->assertEquals(1, $propertyObj->getProperty());
	
		$stubber = Stubber::spyOn($propertyObj);

		$this->assertEquals(1, $stubber->getProperty(), 'Recently created Stubber did not inherit $instance state');

		$stubber->modifyProperty();
	
		$this->assertEquals(2, $stubber->getProperty(), 'Stubber did not take into account $instance state');
		$this->assertEquals(2, $propertyObj->getProperty(), 'Decoration failed!');
	
	}
	
	public function testSpyingAutoStubbedMethod_CallingWithArguments_ReturnsMatchingCallsCount() {
		
		$stubber = Stubber::spyOn(new stdClass);
		$stubber->method(1,2);

		$this->assertEquals(1, $stubber->method->invoked->with(1, 2));
	}
	
	public function testSpyingAutoStubbedMethod_CallingWithArguments_ReturnsZeroIfArgumentsDoNotMatch() {
		$stubber = Stubber::spyOn(new stdClass);

		$stubber->method(1, 3);
		
		$this->assertEquals(0, $stubber->method->invoked->with(1, 2));
	}
	
	public function testSpyingAutoStubbedMethod_CallingWithArguments_ReturnCallCountForAllInvocationsWhenCalledWithAny() {
		$stubber = Stubber::spyOn(new stdClass);
		
		$stubber->method(1, 3);
		$stubber->method(2, 3);
		$stubber->method();

		$callCount = $stubber->method->invoked->withAny();

		$this->assertEquals(3, $callCount);
	}

	public function testSpying_ModifyingPrivateInstanceFieldByTheInstance_ThePrivateFieldReflectsChangesToTheOriginalInstance() {

		$propertyObj = new Examples\ExamplePrivateProperty();
		$propertyObj->modifyProperty();

		$propertyValue = $propertyObj->getProperty();
		$this->assertEquals(1, $propertyValue);

		$stubber = Stubber::spyOn($propertyObj);

		$propertyObj->modifyProperty();

		$propertyValueSeenByTheSpy = $stubber->getProperty();
		$propertyValueSeenByTheInstance = $propertyObj->getProperty();

		$this->assertEquals(2, $propertyValueSeenByTheSpy, 'Recently created Stubber did not inherit $instance state');
		$this->assertEquals(2, $propertyValueSeenByTheInstance);
	}

	public function testSpying_ModifyingPrivateInstanceFieldByTheSpy_TheInstancePrivateFieldsReflectsChangesInTheOriginalInstance2() {
		$propertyObj = new Examples\ExamplePrivateProperty();
		$propertyObj->modifyProperty();

		$propertyValue = $propertyObj->getProperty();
		$this->assertEquals(1, $propertyValue);
		$stubber = Stubber::spyOn($propertyObj);
		$this->assertEquals(1, $stubber->getProperty());


		$stubber->modifyProperty();

		$propertyValueSeenByOriginalObject = $propertyObj->getProperty();
		$propertyValueSeenByTheSpy = $stubber->getProperty();

		$this->assertEquals(2, $propertyValueSeenByOriginalObject, 'Recently created Stubber did not inherit $instance state');
		$this->assertEquals(2, $propertyValueSeenByTheSpy);
	}

	public function testSpying_ModifyThePropertyAfterStartingToSpyOnIt_ChangesMadeByOriginalInstanceAreReflectedInTheSpy() {
		$propertyObj = new Examples\ExamplePrivateProperty();
		$propertyObj->modifyProperty();

		$stubber = Stubber::spyOn($propertyObj);
		$propertyObj->modifyProperty();

		$this->assertEquals(2, $stubber->getProperty());
	}

	public function testSpying_CountingMethodCallsCallsInvokedFromPrivateMethod_TheCallsAreCorrectlyCounted() {
		$instance = new Examples\PrivateCallsAnother();
		$spied = Stubber::spyOn($instance);

		$spied->entryMethod();

		$callcount = $spied->method->called;

		$callcount_private = $spied->privateMethod->called;
		$this->assertEquals(3, $callcount);
		$this->assertEquals(1, $callcount_private);
	}

	public function testSpying_GeneratingProxyDelegatesCorrectly_DelegationWorksCorrectly() {
		$instance = new Examples\DelegatingInProxy();
		$spied = Stubber::spyOn($instance);

		$spied->setX(10);

		$this->assertEquals(10, $spied->abc());
		$this->assertEquals(10, $spied->xyz());
		//$this->assertEquals(10, $spied->cde());
	}
}
