<?php
/*
Copyright (c) 2013-2014, Tomasz Cichecki
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Tomasz Cichecki nor the  names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
/**
 * Stubber
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://bitbucket.org/devello/stubber/master/LICENSE
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to knight@kopernet.org so we can send you a copy immediately.
 *
 * @category Stubber
 * @package Stubber
 * @subpackage UnitTests
 * @copyright Copyright (c) 2013 Tomasz Cichecki (https://bitbucket.org/devello)
 * @license https://bitbucket.org/devello/stubber/master/LICENSE New BSD License
 */

/*
 * Set error reporting to the level to which Stubber code must comply.
*/
error_reporting(E_ALL);

/*
 * Determine the root, library, and tests directories of the framework
* distribution.
*/
$root = realpath(dirname(dirname(__FILE__)));
$library = $root . DIRECTORY_SEPARATOR . 'library';
$tests = $root . DIRECTORY_SEPARATOR . 'tests';
$examples = $root . DIRECTORY_SEPARATOR . '_examples';

defined('TEMP_SOURCE_ROOT') || define('TEMP_SOURCE_ROOT', $root . DIRECTORY_SEPARATOR . 'tmp_source' );
if (!file_exists(TEMP_SOURCE_ROOT)) {
	mkdir(TEMP_SOURCE_ROOT);
}
$tempdir = opendir(TEMP_SOURCE_ROOT);
while(($temp_file = readdir($tempdir)) !== false) {
	if (!in_array($temp_file, array('.', '..')))
		unlink(TEMP_SOURCE_ROOT . DIRECTORY_SEPARATOR . $temp_file);
}
closedir($tempdir);
/**
 * Check that --dev composer installation was done
 */
if (!file_exists($root . '/vendor/autoload.php')) {
    throw new Exception(
            'Please run "php composer.phar install --dev" in root directory '
            . 'to setup unit test dependencies before running the tests'
    );
}

/*
 * Prepend the Mutateme library/ and tests/ directories to the
* include_path. This allows the tests to run out of the box and helps prevent
* loading other copies of the code and tests that would supercede
* this copy.
*/
$path = array(
        $library,
        $tests,
		$examples,
        get_include_path(),
);
set_include_path(implode(PATH_SEPARATOR, $path));

if (defined('TESTS_GENERATE_REPORT') && TESTS_GENERATE_REPORT === true &&
version_compare(PHPUnit_Runner_Version::id(), '3.1.6', '>=')) {

    /*
     * Add Mutateme library/ directory to the PHPUnit code coverage
    * whitelist. This has the effect that only production code source files
    * appear in the code coverage report and that all production code source
    * files, even those that are not covered by a test yet, are processed.
    */
    PHPUnit_Util_Filter::addDirectoryToWhitelist($library);

    /*
     * Omit from code coverage reports the contents of the tests directory
    */
    foreach (array('.php', '.phtml', '.csv', '.inc') as $suffix) {
        PHPUnit_Util_Filter::addDirectoryToFilter($tests, $suffix);
    }
    PHPUnit_Util_Filter::addDirectoryToFilter(PEAR_INSTALL_DIR);
    PHPUnit_Util_Filter::addDirectoryToFilter(PHP_LIBDIR);
}

require __DIR__.'/../vendor/autoload.php';

/*
 * Unset global variables that are no longer needed.
*/
unset($root, $library, $tests, $path, $tempdir, $temp_file);
Stubber::setFileStore();


