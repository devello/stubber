<?php
/**
Copyright (c) 2013-2014, Tomasz Cichecki
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Tomasz Cichecki nor the  names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

class StubTest extends PHPUnit_Framework_TestCase {
	public function types() {
	    return array(
	            array('\\stdClass'),
	            array('\\Examples\\Greeter')
	    );
	}
	
	/**
	 * @dataProvider types
	 */
	public function testStub_CreateStub_IsOfTheSameTypeAsOriginalObject($expected) {
		$stub = Stubber::stub($expected);
		$this->assertInstanceOf($expected, $stub);
		$this->assertTrue(is_subclass_of($stub, $expected));
	}

	public function testStub_CallingMethods_NoSideEffectsOfTheStubbedMethod() {
		$stub = Stubber::stub("\\Examples\\ExampleWithManyMethods");

		$stub->method4();
	}

	public function testStub_CallingMethods_MethodReturnValueCanBeStubbed() {
		$stub = Stubber::stub("stdClass");
		$stub->method->returns(5);

		$result = $stub->method();

		$this->assertEquals(5, $result);
	}
	
	public function testStub_MissingConstructorArguments_NewInstanceWithoutConstructorIsCreated() {
		$stub = Stubber::stub("Examples\\MandatoryConstructorParameters");
		$dummy = Stubber::dummy("Examples\\MandatoryConstructorParameters");
	}
	
	public function testStub_ClosureArguments_ReflectedClosureNameIsUsedInstead() {
		$stub = Stubber::stub("Examples\\ThisInVarName");
		$stub->method->returns('stubbed');
		$return_value = $stub->method(function() {});
		
		$this->assertEquals('stubbed', $return_value);
	}
	
	public function testRegisterReturnWhenCalledAtSpecificLocation_LocationMatches_ReturnValueIsReturned() {
		$stub = Stubber::stub("stdClass");
		$stub->method->calledFrom(__FILE__, __LINE__ + 2)->returns(5);
		
		$returnValue = $stub->method();
		
		$this->assertEquals(5, $returnValue);
	}
	
	public function testRegisterReturnWhenCalledAtSpecificLocation_LocationDoesNotMatch_ReturnValueIsNotReturned() {
		$stub = Stubber::stub("stdClass");
		$stub->method->calledFrom(__FILE__, __LINE__)->returns(5);
		
		$returnValue = $stub->method();
		
		$this->assertNotEquals(5, $returnValue);
	}
	
	public function testRegisterThrowsWhenCalledAtSpecificLocation_LocationDoesNotMach_NoExceptionIsThrown() {
		$stub = Stubber::stub("stdClass");
		
		$stub->method->calledFrom(__FILE__, __LINE__)->throws(new \Stubber\Exceptions\AccessViolation());
		
		$stub->method();
	}
	
	public function testRegisterThrowsWhenCalledAtSpecificLocation_LocationMatches_RegisteredExceptionIsThrown() {
		$stub = Stubber::stub("stdClass");
		
		$stub->method->calledFrom(__FILE__, __LINE__ + 2)->throws(new \Stubber\Exceptions\AccessViolation());
		try {
			$stub->method();
			$this->fail("No exception thrown");
		}
		catch (\Stubber\Exceptions\AccessViolation $e) { return; }
		catch (Exception $e) { $this->fail("Unexpected excepton type " . get_class($e) . " thrown"); }
	}
	
	public function testStubbingAtSpecificLocation_LocationDoesNotMatch_DefaultStubValueIsReturned() {
		$stub = Stubber::stub("stdClass");
		
		$stub->method->calledFrom(__FILE__, __LINE__)->stubs(function () { return "stubbed!";});
		
		$result = $stub->method();
		
		$this->assertNotEquals("stubbed!", $result);
	}
	
	public function testStubbingAtSpecificLocation_LocationMatches_RegisteredCallbackIsCalled() {
		$stub = Stubber::stub("stdClass");
		
		$stub->method->calledFrom(__FILE__, __LINE__ + 2)->stubs(function () { return "stubbed!";});
		
		$result = $stub->method();
		
		$this->assertEquals("stubbed!", $result);
	}
	
	public function testStubbingAtSpecificLocation_LocationMatches_RegisteredCallbackIsCalledSet3() {
		$stub = Stubber::stub("stdClass");
		
		$stub->method->calledFrom(__FILE__, __LINE__ + 3)->stubs(function () { return "stubbed1"; });
		$stub->method->calledFrom(__FILE__, __LINE__ + 3)->stubs(function () { return "stubbed2"; });
		
		$result = $stub->method();
		$result2 = $stub->method();
		
		$this->assertEquals("stubbed1", $result);
		$this->assertEquals("stubbed2", $result2);
	}
	
	public function testStubbingAtSpecificLocation_LocationMatches_RegisteredCallbackIsCalledSet4() {
		$stub = Stubber::stub("stdClass");
		
		$stub->method->calledFrom(__FILE__, __LINE__ + 3)->stubs(function() { return "stubbed at specific location!"; });
		$stub->method->stubs(function() { return "stubbed with a default callback"; });
		
		$specificReturnValue = $stub->method();
		$defaultValue = $stub->method();
		
		$this->assertEquals("stubbed at specific location!", $specificReturnValue);
		$this->assertEquals("stubbed with a default callback", $defaultValue);
	}
	
	public function testStubbingAtSpecificLocation_LocationMatches_RegisteredCallbackIsCalledSet2() {
		$stub = Stubber::stub("stdClass");
		$stub->method->calledFrom(array('function'=>'testStubbingAtSpecificLocation_LocationMatches_RegisteredCallbackIsCalledSet2','line'=>__LINE__ + 2))->stubs(function () { return "stubbed!";});
	
		$result = $stub->method();
		
		$this->assertEquals("stubbed!", $result);
	}
	
	/**
	 * @dataProvider with
	 * @param mixed $callArg
	 */
	public function testRegisterReturn_MatchingArgument_TheCallShouldYieldRegisteredValue($callArg) {
		$stub = Stubber::stub('stdClass');
		$stub->method->with($callArg)->returns('registered return value');
		
		$returnValue = $stub->method($callArg);
		
		$this->assertEquals('registered return value', $returnValue);
	}
	
	public function with() {
		$recursiveObject = new stdClass;
		$recursiveObject->recursiveObject = $recursiveObject;
		
		$recursiveArray = array(
			'recursiveArray'=>&$recursiveArray
		);
		return array(
			array(function() {}),
			array($recursiveObject),
			array($recursiveArray)
		);
	}
	
	public function testStubbingAtSpecificLocation_BacktrackLocatorMatches_RegisteredCallbackIsCalled() {
		$stub = Stubber::stub("stdClass");
		
		$stub->method->calledFrom(array('class'=>'StubTest', 'function'=>'testStubbingAtSpecificLocation_BacktrackLocatorMatches_RegisteredCallbackIsCalled'))->stubs(function () { return "stubbed!";});
		
		$result = $stub->method();
		
		$this->assertEquals("stubbed!", $result);
	}
	
	public function testStubbingAtSpecificLocation_BacktrackMatcherIsEmpty_RegisteredCallbackIsNotCalled() {
		$stub = Stubber::stub("stdClass");
		
		$stub->method->calledFrom(array())->stubs(function () { return "stubbed!";});
		
		$result = $stub->method();
		
		$this->assertNotEquals("stubbed!", $result);
	}
	
	public function testRegisterReturnWhenCalledAtSpecificLocation_LocationDoesNotMatchAnotherReturnIsRegistered_ReturnValueIsNotReturned() {
		$stub = Stubber::stub("stdClass");
		$stub->method->calledFrom(__FILE__, __LINE__)->returns(5);
		$stub->method->returns(7);
		$returnValue = $stub->method();
		
		$this->assertEquals(7, $returnValue);
	}

	public function testRegisterReturnValueWhenCalledAtSpecificLocation_MultipleStackFramesMatch_TopOfTheStackIsMoreImportant() {
		$stub = Stubber::stub("stdClass");
		$stub->method->calledFrom(array('class'=>'Examples\\CallStack', 'function'=>'fromHere'))->returns(5);
		$stub->method->calledFrom(array('class'=>'Examples\\CallStack', 'function'=>'fromThere'))->returns(7);

		$environment = new \Examples\CallStack($stub);
		$result = $environment->fromHere();

		$this->assertEquals(7, $result);
	}

	public function testRegisterReturnValueWhenCalledAtSpecificLocation_SpecifiesFileNumber_RegisteredReturnValueIsReturned() {
		$stub = Stubber::stub("stdClass");
		$stub->method->calledFrom(array('class'=>'Examples\\CallStack', 'function'=>'fromHere'))->returns(5);
		$stub->method->calledFrom(array('class'=>'Examples\\CallStack', 'function'=>'fromThere', 'line'=>20))->returns(7);

		$environment = new \Examples\CallStack($stub);
		$result = $environment->fromHere();

		$this->assertEquals(7, $result);
	}

	public function testRegisterReturnValueWhenCalledAtSpecificLocation_SpecifiesFilename_RegisteredReturnValueReturned() {
		$stub = Stubber::stub("stdClass");
		$stub->method->calledFrom(array('class'=>'Examples\\CallStack', 'function'=>'fromHere'))->returns(5);
		$stub->method->calledFrom(array('class'=>'Examples\\CallStack', 'function'=>'fromThere', 'file'=>'CallStack.php'))->returns(7);

		$environment = new \Examples\CallStack($stub);
		$result = $environment->fromHere();

		$this->assertEquals(7, $result);
	}
	
	public function testRegisterReturnValue_OverridingReturnValue_ReturnValueCanBeOverridden()
	{
		$stub = Stubber::stub("stdClass");
		$stub->method->returns(1);
		$stub->method->returns(2);
		
		$result = $stub->method();
		
		$this->assertEquals(2, $result);
	}
	
	public function testStubImplementsInterfaces_FinalClassImplementingAnInterface_TheDuckTypeImplementsTheInterface() {
		$stub = Stubber::stub('Examples\\FinalImplementing');
		
		$this->assertInstanceOf('Examples\\AbstractTypes\\Iface', $stub);
	}
	
	public function testStubImplementsInterfaces_FinalClassExtendingAnImplementor_TheDuckTypeImplements() {
		$stub = Stubber::stub('Examples\\FinalExtendingAnImplementor');
		
		$this->assertInstanceOf('Examples\\AbstractTypes\\Iface', $stub);
	}
}
