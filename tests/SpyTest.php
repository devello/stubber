<?php
/*
Copyright (c) 2013-2014, Tomasz Cichecki
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Tomasz Cichecki nor the  names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

use Stubber\Spy;

class SpyTest extends PHPUnit_Framework_TestCase
{
	public function testCallingMethods_CallingAnyMethod_ReturnTheSpyObject() {
		$spy = new Spy();

		$this->assertSame($spy, $spy->call());
	}

	public function testAccessingPropertys_InstanceProperty_ReturnInstancePropertyValue() {
		$spied = new \stdClass;
		$spied->myProperty = 5;

		$spy = new Spy($spied);

		$property_value = $spy->myProperty;

		$this->assertEquals(5, $property_value);
	}

	public function testCalling_RegisteringReturnValue_TheValueIsReturn() {

		$spy = new Spy();
		$spy->call->returns(5);

		$return_value = $spy->call();

		$this->assertEquals(5, $return_value);
	}

	public function testCalling_RegisteringExceptionToBeThrown_ExceptionIsThrown() {

		$spy = new Spy();
		$spy->call->throws(new \Stubber\Exceptions\UnknownMethod());

		try {
			$spy->call();
			$this->fail();
		} catch (\Stubber\Exceptions\UnknownMethod $e) {
			return;
		}
		$this->fail();
	}


	public function testCalling_RegisteringMultipleReturnValues_ValuesAreReturnedBasedOnArguments() {

		$spy = new Spy();

		$spy->exampleMethod->with(1, 2)->returns(1);
		$spy->exampleMethod->with(3, 4)->returns(2);
		$spy->exampleMethod->with(4, 5)->stubs(function ($arg1, $arg2){ return 5; });
		$spy->exampleMethod->stubs(function() { return 6; });

		$forth_return_value = $spy->exampleMethod();
		$first_return_value = $spy->exampleMethod(1, 2);
		$second_return_value =$spy->exampleMethod(3, 4);
		$thrird_return_value =$spy->exampleMethod(4, 5);


		$this->assertEquals(1, $first_return_value);
		$this->assertEquals(2, $second_return_value);

		$this->assertEquals(5, $thrird_return_value);
		$this->assertEquals(6, $forth_return_value);
	}

	public function testCalling_RegisteringACallbackAsReturnValue_CallbackShouldBeReturnedNotRun()
	{
		$spy = new Spy();

		$spy->exampleMethod->returns(function () {});

		$return_value = $spy->exampleMethod();
		$second_return_value = $spy->exampleMethod(3);

		$this->assertTrue(is_callable($return_value));
		$this->assertTrue(is_callable($second_return_value));
	}

}
