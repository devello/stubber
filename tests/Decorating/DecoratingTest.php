<?php
/*
Copyright (c) 2013-2014, Tomasz Cichecki
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Tomasz Cichecki nor the  names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
namespace Decorating;

use stdClass,
	ReflectionClass,
	PHPUnit_Framework_TestCase,
	Examples,
	Decorator,
	Stubber;

class DecoratingTest extends PHPUnit_Framework_TestCase {
	public function setUp() {
		parent::setUp();

		Decorator::setFileStore();
	}
	public function testDecoratedObjectsTypeIsOfTheDecoratedClass() {
		$original = new Examples\OneLiner();

		$decorated = Decorator::decorate($original, 'Decorator\\DummyDecorator');
		self::assertInstanceOf('Examples\\OneLiner', $decorated);
	}

	public function testDecoratoringClassIsASubclassOfTheDecoratedObjectsClass() {
		$original = new Examples\OneLiner();
		$decorated = Decorator::decorate($original);

		self::assertTrue(is_subclass_of($decorated, 'Examples\\OneLiner'));
	}

	public function testDecoratorDelegatesCallsToTheDecoratedInstance() {
		
		$spy = $this->getMock('stdClass', array('commitCall'));
		$spy->expects($this->once())->method('commitCall');

		$spied = new Examples\SpiedObject($spy);
		$decorated = Decorator::decorateWith($spied, 'Decorator\\DummyDecorator');

		$decorated->method();

	}
	
	public function testDecoratorDelegatesCallsToTheDecoratedInsanceDedicatedDecorator() {
		
		$spy = $this->getMock('stdClass', array('commitCall'));
		$spy->expects($this->once())->method('commitCall');
		
		$spied = new Examples\SpiedObject($spy);
		$decorated = Decorator::decorateWith($spied, 'Examples\\Decorator\\SpiedObjectDecorator');
		
		$decorated->method();
	}
	
	public function testDecoratorBehavesCorrectlyWhenUsingAnObjectWithFields() {
		$original = new Examples\ExamplePrivateProperty;
		
		$decorated = Decorator::decorateWith($original, 'Decorator\\DummyDecorator');
		$decorated->modifyProperty();
		
		$this->assertEquals(1, $decorated->getProperty());
		$this->assertEquals(1, $original->getProperty());
	}

	public function testCanDecorateUsingExistingDecoratorType() {
		$instance = new stdClass;
		$decorated = Decorator::decorateWith($instance, 'Examples\\Decorator\\Traiting');

		$this->assertEquals("This method has been added", $decorated->greet());

	}
	
	public function testCanGenerateProxyForReferenceReturningFunction() {
		$reflection = \Devello\ReflectionClass::getReflection('Examples\ArgumentsPassedByReference');
		$source = \Stubber::generateProxySource($reflection);
		$this->assertContains('&passedAndReturnedByReference', $source);
	}
	
	public function testGeneratedSourceHasProperSignatureForReferenceReturningMethods() {
		$reflection = \Devello\ReflectionClass::getReflection('Examples\ArgumentsPassedByReference');
		$source = Decorator::generateDecoratorSource($reflection, 'Proxy_my', 'Decorator\\DummyDecorator');
		$this->assertContains('&passedAndReturnedByReference', $source);
		$this->assertContains('&$this->_instance->', $source);
	}
	
	public function testDecoratorCanEmitSourceCodeForDecoratedObjects() {
		$obj = Decorator::decorateWith(new Examples\ExampleWithPrivateMethods(), 'Decorator\\DummyDecorator');
		$this->assertContains(get_class($obj), Decorator::getSource($obj));
	}
	
	public function testDecoratorCopiesPropertyDefinitionsFromUsedDecoratorType() {
		$obj = Decorator::decorateWith(new Examples\ExampleWithPrivateMethods(), 'Examples\\Decorator\\DecoratorWithField');
		$this->assertContains('public $_calls', Decorator::getSource($obj));
		
	}
	
	public function testIncorrectMethodSignature() {
		$obj = Decorator::decorateWith(new Examples\ExamplePrivateProperty());
		$this->assertContains('public function modifyReference()', Decorator::getSource($obj));
	}
	
	public function testCanDecorateAnObjectWhichTypeHasADefinedConstructor() {
		$obj = Decorator::decorate(new Examples\ExampleWithConstructor());
		
	}
	
	public function testExclaimationDecoratorExample() {
		$obj = Decorator::decorateWith(new Examples\Greeter('Tomek'), 'Examples\\Decorator\\ExclamationDecorator');
		
		$this->assertEquals('hello Tomek!', $obj->greet());
	}
	
	public function testDecoratorCanExtendDecoratedClass() {
		
		$obj = Decorator::decorateWith(new Examples\Greeter('Tomek'), 'Examples\\Decorator\\QuestionMarkDecorator');
		
		$this->assertEquals('hello Tomek?', $obj->greet());
	}

	public function testDecoratorDoesNotEmitTheTemplate() {
		$obj = Decorator::decorateWith(new Examples\ExampleWithPrivateMethods(), 'Decorator\\DummyDecorator');

		$this->assertNotContains('__delegate_template__', Decorator::getSource($obj));
	}

	public function testDecoratingClassNameCanBeDefined() {
		$decoratorClassName = Decorator::generateDecorator('Examples\\OneLiner', 'MyHappyDecorator', 'Decorator\\DummyDecorator');
		$this->assertEquals('MyHappyDecorator', $decoratorClassName);
	}
	
	public function testDecoratingMethodsWithHints() {
		$decoratorClassName = Decorator::generateDecorator('Examples\\Subject', 'MyHappyDecorator2', 'Decorator\\DummyDecorator');
		$this->assertContains('(\\SplObserver $', Decorator::getSource($decoratorClassName)); 
	}
	
	public function testDecoratingMethodsWithDefaultValue() {
		$decoratorClassName = Decorator::generateDecorator('Examples\\Subject', 'MyHappyDecorator3', 'Decorator\\DummyDecorator');
		$this->assertContains('$arg0=NULL', Decorator::getSource($decoratorClassName));
	}

	public function testDecoratingAbstractClassCanInstaniateTheDecorator() {
		Decorator::generateDecorator('Examples\\AbstractTypes\\AbstractClass', 'AnAbstractClassDeco1', 'Decorator\\DummyDecorator');

		new \AnAbstractClassDeco1;
	}

	public function testDecoratingAbstractClassCanInvokeTheAbstractMethod() {
		$decoratorClassName = Decorator::generateDecorator('Examples\\AbstractTypes\\AbstractClass', 'AnAbstractClassDeco2', 'Decorator\\DummyDecorator');

		$this->assertEquals($decoratorClassName, 'AnAbstractClassDeco2');
		$instance = new $decoratorClassName;
		$instance->abstractMethod();
	}

	public function testDecoratingAbstractClassCanInvokeNonAbstractMethod() {
		$decoratorClassName = Decorator::generateDecorator('Examples\\AbstractTypes\\AbstractClass', 'AnAbstractClassDeco3', 'Decorator\\DummyDecorator');

		$instance = new $decoratorClassName;

		$instance->nonAbstractMethod();
	}

	public function testDecoratingAbstractClassWithConstructor() {
		$decoratorClassName = Decorator::generateDecorator('Examples\\AbstractTypes\\AbstractClassWithConstructor', null, 'Decorator\\DummyDecorator');
		$instance = new $decoratorClassName(1, 2, 3);
		$this->assertEquals(1, $instance->_a);
		$this->assertEquals(2, $instance->_b);
		$this->assertEquals(3, $instance->_c);
	}

	public function testDecoratingBuiltInClass() {
		$instance = new \DateTime();
		Decorator::decorateWith($instance, '\\Decorator\\DummyDecorator');

	}

	public function testDecoratingFinalClass() {
		$instance = new Examples\FinalClass();
		$duckTypeInstance = Decorator::decorateWith($instance);
	}

	public function testDecoratingClassWithFinalMethods() {
		$instance = new Examples\FinalMethod();
		$duckTypeInstance = Decorator::decorateWith($instance);
		$this->assertNotInstanceOf('Examples\\FinalMethod', $duckTypeInstance);
		$this->assertTrue(method_exists($duckTypeInstance, 'finalMethod'));
	}
	
	public function testDecorating_VariableParams_ItsOk() {
		$instance = new Examples\Params();
		$decorator = Decorator::decorateWith($instance);
		
		$result = $decorator->params(5);
		
		$this->assertEquals(5, $result);
	}
}
