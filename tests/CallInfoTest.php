<?php
/*
Copyright (c) 2013-2014, Tomasz Cichecki
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Tomasz Cichecki nor the  names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

use Stubber\CallInfo;

class CallInfoTest extends PHPUnit_Framework_TestCase {
	public function testQueringMatchingCalls_NothingRecorded_NeverCalledWithReturnsTrue() {
		$matcher = $this->_matcher();

		//$spy->exampleMethod->neverCalledWith(1, 3)
		$this->assertTrue($matcher->neverCalledWith(1, 3));
	}

	public function testQueryingMatchingCalls_MatchingArgumentsRecording_NeverCalledWithReturnsFalse() {
		$matcher = $this->_matcher();
		$matcher->args[] = array(1, 3);

		$this->assertFalse($matcher->neverCalledWith(1, 3));
	}

	public function testQueryingMatchingCalls_NotMatchingArgumentsRecorded_NeverCalledReturnsTrue() {
		$matcher = $this->_matcher();
		$matcher->args[] = array(0, 0);

		$this->assertTrue($matcher->neverCalledWith(1, 3));
	}

	public function testQueryingMatchingCalls_MatchingArgumentsRecorded_CalledOnceReturnsTrue() {
		$matcher = $this->_matcher();
		$matcher->args[] = array(1);

		$this->assertTrue($matcher->onceCalledWith(1));
		$this->assertTrue($matcher->calledOnceWith(1));
	}

	public function testQueryingMatchingCalls_ExpectedArgumentsPassedByInWrongOrder_CalledOnceReturnsFalse() {
		$matcher = $this->_matcher();

		$matcher->args[] = array(2, 1);

		$this->assertFalse($matcher->onceCalledWith(1, 2));
	}

	public function testQueryingMatchingCalls_ArgumentsRecorded_CalledReturnsTrue() {
		$matcher = $this->_matcher();

		$matcher->args[] = array(1, 2);

		$this->assertTrue($matcher->called());
	}

	public function testQueryingMatchingCalls_NoArgumentsRecorded_CalledConsidersAnyCallsEverOccured() {
		$matcher = $this->_matcher();

		$this->assertFalse($matcher->called());
	}

	public function testQueryingMatchingCalls_CallingMultipleTimes_CallWithMatchingArgumentsOccuredAtSpecifiedIndex() {
		$matcher = $this->_matcher();

		$matcher->args[] = array(0, 1);
		$matcher->args[] = array(1, 2);
		$matcher->args[] = array(2, 3);

		$this->assertTrue($matcher->calledAt_0_with(0, 1));
		$this->assertTrue($matcher->calledAt_1_with(1, 2));

		$this->assertFalse($matcher->calledAt_1_with(2, 2));

		$this->assertEquals(1, $matcher->calledAt(1)->with(1, 2));

	}

	public function partialMatching() {
		return array(
			array(array(1=>2), 1),
			array(array(1=>-1), 0),
			array(array(1=>3), 2),
			array(array(0, 1), 1)
		);
	}
	/**
	 * @dataProvider partialMatching
	 * @param $expected
	 * @param $parameterMatch
	 */
	public function testQueryingMatchingCalls_PartialMatches_MatchIndicatesNumberTimes($parameterMatch, $expected) {
		$matcher = $this->_matcher();

		$matcher->args[] = array(0, 1);
		$matcher->args[] = array(1, 2);
		$matcher->args[] = array(2, 3);
		$matcher->args[] = array(1, 3);

		$this->assertEquals($expected, $matcher->withArguments($parameterMatch));
	}
	/**
	 * @return \Stubber\CallInfo
	 */
	private function _matcher()
	{
		$matcher = new CallInfo();
		return $matcher;
	}
}
