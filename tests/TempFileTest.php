<?php
/*
Copyright (c) 2013-2014, Tomasz Cichecki
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Tomasz Cichecki nor the  names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

class TempFileTest extends PHPUnit_Framework_TestCase {

	public function testShouldAutomaticallyBeDeletedWhenThereAreNoReferencesToIt() {
		$file = new Decorator\TempFile('file.txt');
		
		$file = null;
		
		$this->fileExists($this->tempFilesDir() . 'file.txt');
	}

	public function testFileShouldExistAsLongAsAReferenceToItExists() {
		$file = new Decorator\TempFile('file.txt');
		$this->fileExists($this->tempFilesDir() . 'file.txt');
	}

	public function testFileShouldBeCreatedWithTheFilenameGiven() {
		$file = new Decorator\TempFile('file.php');
		
		$this->fileExists($this->tempFilesDir() . 'file.php');
	}

	public function testUnlinkingBeforeDestroyingTheHandleShouldNotCauseAnyExceptionsNorWarning() {
		$file = new Decorator\TempFile('file.test');
		
		fclose($file->file);
		unlink($this->tempFilesDir() . 'file.test');
		unset($file);
		
	}

	public function testTempFileShouldKnowHowToStoreTheContent() {
		$file = new Decorator\TempFile('file2.txt');
		
		$file->store('this text has been stored');
		$text = file_get_contents($this->tempFilesDir() . 'file2.txt');

		$this->assertEquals('this text has been stored', $text);
	}
	
	private function tempFilesDir() {
		return TEMP_SOURCE_ROOT . DIRECTORY_SEPARATOR;
	}
}
