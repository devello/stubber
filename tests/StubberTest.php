<?php
/*
Copyright (c) 2013-2014, Tomasz Cichecki
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Tomasz Cichecki nor the  names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

class StubberTest extends PHPUnit_Framework_TestCase {

	public function typeCases() {
		return array(
			array('Stubber'),
			array('Examples\\Oneliner')
		);
	}

	public function callCountCases() {
		return array(
			array(0),
			array(1),
			array(2),
			array(10)
		);
	}

	/**
	 * @dataProvider typeCases
	 */

	public function testStubberIsOfRequestedType($expected_type) {
		$stubber = Stubber::create($expected_type);

		self::assertInstanceOf($expected_type, $stubber);
	}

	/**
	 * @dataProvider typeCases
	 */
	public function testAnythingCalledReturnsItself($type) {
		$stubber = Stubber::create($type);

		self::assertSame($stubber, $stubber->exampleMethod());
	}

	public function testCallsAreChainableAndFluentByDefault() {
		$stubber = Stubber::create("Examples\\OneLiner");

		self::assertInstanceOf("Examples\\OneLiner", $stubber->exampleMethod()->anotherMethod());
	}

	
	/**
	 * @dataProvider callCountCases
	 */
	public function testCallsToMethodsAreCounted($expected_callcount) {
		
		$stubber = Stubber::create();
		
		for($i=0; $i<$expected_callcount; ++$i) {
			$stubber->exampleMethod();
		}
		
		$this->assertEquals($expected_callcount, $stubber->exampleMethod->called);
	}

	/**
	 * @dataProvider callCountCases
	 */
	public function testCallsToMethodOnExistingMethodsOfDecoratedClassesAreCounted($expected_callcount) {
		
		$stubber = Stubber::create('Examples\\ExampleWithManyMethods');
		
		for($i=0; $i<$expected_callcount; ++$i) {
			$stubber->exampleMethod();
		}
		
		$this->assertEquals($expected_callcount, $stubber->exampleMethod->called);
	}

	public function testStubberCanRegisterReturnValue() {
		$stub = Stubber::create();

		$stub->exampleMethod->returns("example return value");

		$this->assertEquals("example return value", $stub->exampleMethod());
	}

	public function testStubberCanRegisterReturnValueForExistingMethods() {
		$stub = Stubber::create("Examples\\ExampleWithManyMethods");

		$stub->method1->returns("another example value");

		$this->assertEquals("another example value", $stub->method1());
	}

	public function testStubberCanRegisterSpecificReturnTypeForGivenArguments() {
		$stub = Stubber::create();

		$stub->exampleMethod->with(1, 2)->returns("this invocation has been stubbed!");

		$this->assertEquals('this invocation has been stubbed!', $stub->exampleMethod(1, 2));
	}

	public function testStubberCanRegisterSpecificSequenceOfReturnValues() {
		$stub = Stubber::create();

		$stub->exampleMethod->returns_on_subsequent_calls(1, 2, 3, 4, 5);

		$this->assertEquals(1, $stub->exampleMethod());
		$this->assertEquals(2, $stub->exampleMethod());
		$this->assertEquals(3, $stub->exampleMethod());
		$this->assertEquals(4, $stub->exampleMethod());
		$this->assertEquals(5, $stub->exampleMethod());
	}
	
	public function testStubberFallsBackToDefaultReturnValueAfterExaustingReturnValuesSequence() {
		$stub = Stubber::create();

		$stub->exampleMethod->returns_on_subsequent_calls(1, 2, 3);
		$stub->exampleMethod();
		$stub->exampleMethod();
		$stub->exampleMethod();

		$result = $stub->exampleMethod();
		$this->assertEquals($stub, $result);
	}

	public function testStubberCanRegisterToThrowExceptionOnCall() {
		$stub = Stubber::create();
		$stub->exampleMethod->throws(new \Examples\Exceptions\ExampleException);

		try {
			$stub->exampleMethod();
			$this->fail('No expected exception was thrown');
		} catch (\Examples\Exceptions\ExampleException $e) {
			return;
		}
	}

	/**
	 * @dataProvider callCountCases
	 */
	public function testStubberCanCountCallsOnSpiedProtectedMethods($expected_callcount) {
		$stub = Stubber::create('Examples\\ExampleWithProtectedMethods');
		
		for ($i=0; $i<$expected_callcount; ++$i) {
			$stub->method_public_that_calls_protected_method();
		}

		$actual_callcount = $stub->method_protected->called;

		$this->assertEquals($expected_callcount, $actual_callcount);
	}

	/**
	 * @dataProvider callCountCases
	 */
	public function testStubberCanCountCallsToSpysPrivateMethods($expected_callcount) {
		$stub = Stubber::create("Examples\\ExampleWithPrivateMethods");
		
		for ($i=0; $i < $expected_callcount; ++$i) {
			$stub->method_public();
		}

		$callCount = $stub->method_private->called;

		$this->assertEquals($expected_callcount, $callCount, "Method `method_private` call was not recorded");
	}

	public function testStubberExtractsFromItselfOnlyThingsThatAreNecessary() {
		$source = Stubber::__get_class_source__();
		$this->assertNotContains('__get_class_source__', $source);
		$this->assertNotContains('__delegate_template__', $source);
		$this->assertNotContains('__delegate_template_private__', $source);
	}
	
	

	public function testArgumentsPassedAndReturnedByReferenceUsingProxy() {

		$obj = new Examples\ProxyArgumentsPassedByReference();
		$value = 10;
		$ref_to_value = &$obj->passedAndReturnedByReference($value);
		$reflector = new ReflectionClass('Examples\\ProxyArgumentsPassedByReference');
		
		$m = $reflector->getMethod('passedAndReturnedByReference');
		$this->assertTrue($m->returnsReference());
		$ref_to_value += 1;
		
		$this->assertEquals(11, $value, 'Failure to document PHP behavior');
	}
	
	public function testArgumentsPasssedAndReturnedByReferenceUsingDecorator() {
		$hardcoded_example = new Examples\Decorator\Proxy\ArgumentsPassedByReference();
		$hardcoded_example->_instance = new Examples\ArgumentsPassedByReference();
		$value = 10;
		$ref_to_value = &$hardcoded_example->passedAndReturnedByReference($value);
		$ref_to_value += 1;
		
		$this->assertEquals(11, $value, 'The hardcoded example does not handle well return by reference');
	}

	public function testCreatingFakeWithInaccessibleConstructor() {
		$stubber = Stubber::create('Examples\\ProtectedConstructor');

		$this->assertInstanceOf('Examples\\ProtectedConstructor', $stubber);
	}

	public function testCreatingFakeFromFinalClass() {
		$stubber = Stubber::create('Examples\\FinalMethod');

	}

	public function testCreatingFakeStarangeNamesOfVariables() {
		$stubber = Stubber::create('Examples\\ThisInVarName');
	}

	public function testAccessingProtectedPropertyOfClassWithFinalMethod() {
		$stubber = Stubber::create('Examples\\DuckWithProtected');

		$stubber->getField();
	}

	public function testAccessingPropertiesOfFinalClass() {
		$stubber = Stubber::create('Examples\\FinalClass');

		$stubber->getField();
	}

	public function testTemplateMethodsAreStrippedWhenCreatingStubber() {
		$stubber = Stubber::create('\\stdClass');

		$this->assertFalse(method_exists($stubber, '__delegate_template__'));
	}
}
